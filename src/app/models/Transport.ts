import {Hangar} from "./Hangar";

export interface Transport {
  type: string;
  capacity: string;
  flightRange: string;
  model: string;
  tailNumber?: string;
  spaceLeft: string;
  hangar?: Hangar;
}
