export interface Hangar{
  hangarId?: number;
  color: string;
  hangarType: string;
}
