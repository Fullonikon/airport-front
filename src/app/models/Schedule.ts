import {Pilot} from "./Pilot";
import {Transport} from "./Transport";

export interface Schedule {
  flightId?: number;
  departureTime: Date;
  transport?: Transport;
  pilot?: Pilot;
}
