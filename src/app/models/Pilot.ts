export interface Pilot{
  employeeId?: number;
  firstName: string;
  lastName: string;
  category: string;
  login: string;
  password: string;
  role: string;
}
