import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppComponent} from "./app.component";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {MatSliderModule} from "@angular/material/slider";
import {CdkTableModule} from "@angular/cdk/table";
import {MatTableModule} from "@angular/material/table";
import {RouterModule, Routes} from "@angular/router";
import { TransportComponent } from "./pages/transport/transport.component";
import { HangarComponent } from "./pages/hangar/hangar.component";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatNativeDateModule} from "@angular/material/core";
import {MatDatepickerModule} from "@angular/material/datepicker";
import { MainComponent } from "./pages/main/main.component";
import { LoginComponent } from "./pages/login/login.component";
import {HeaderInterceptor} from "./interceptors/header.interceptor";
import { PilotsComponent } from "./pages/pilots/pilots.component";
import {AuthGuard} from "./guards/auth.guard";
import { ScheduleComponent } from "./pages/schedule/schedule.component";

const routes: Routes = [  // пути к страницам
  {path: "", component: MainComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ADMIN", "PILOT"]}},
  {path: "schedules", component: ScheduleComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ADMIN", "PILOT"]}},
  {path: "transports", component: TransportComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ADMIN"]}},
  {path: "pilots", component: PilotsComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ADMIN"]}},
  {path: "hangars", component: HangarComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ADMIN"]}},
  {path: "login", component: LoginComponent, canActivate: [AuthGuard], data: {necessaryRoles: ["ANY"]}}
];

@NgModule({
  declarations: [
    AppComponent,
    TransportComponent,
    HangarComponent,
    MainComponent,
    LoginComponent,
    PilotsComponent,
    ScheduleComponent
  ],
  imports: [  // модули
    BrowserModule,
    HttpClientModule,
    MatSliderModule,
    CdkTableModule,
    MatTableModule,
    RouterModule.forRoot(routes),
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatDatepickerModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
