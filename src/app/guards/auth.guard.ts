import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";
import {AuthService} from "../service/auth.service";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> | boolean | UrlTree { // проверка на то что пользователь авторизован, если нет то кидает на логин
    const can = route.data.necessaryRoles.includes(this.authService.getRole()) || route.data.necessaryRoles.includes("ANY");
    if (!can) {
      this.router.navigateByUrl("/login");
    }
    return can;
  }

}
