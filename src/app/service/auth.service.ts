import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthResponse} from "../models/response/AuthResponse";
import {JwtHelperService} from "@auth0/angular-jwt";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  url = "http://localhost:8081/";

  constructor(private http: HttpClient) { }

  public auth(login: string, password: string): Observable<AuthResponse> { // аутентификация по логину и паролю
    return this.http.post<AuthResponse>(this.url + "auth", {login, password});
  }

  public saveToken(token: string): void { // запись токена в сторардж
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(token);
    localStorage.setItem("role", decodedToken.role);
    localStorage.setItem("token", token);
  }

  public getToken(): string {
    return localStorage.getItem("token");
  }

  public getRole(): string {
    return localStorage.getItem("role");
  }

  public clear(): void{
    localStorage.clear();  // очистака локал сторадж браузера для логаута
  }

}
