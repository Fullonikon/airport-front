import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Transport} from "../models/Transport";
import {ObjectUnsubscribedError, Observable} from "rxjs";
import {Hangar} from "../models/Hangar";
import {Pilot} from "../models/Pilot";

@Injectable({
  providedIn: "root",
})
export class AdminService {
  url = "http://localhost:8081/admin/"; // юрл для админа

  constructor(private http: HttpClient) {
  }

  saveTransport(transport: Transport, hangarId: string): Observable<any> {
    return this.http.post(this.url + "saveTransport", {
      transport,
      hangarId
    });
  }

  savePilot(pilot: Pilot): Observable<any> {
    return this.http.post(this.url + "savePilot",  pilot);
  }

  saveHangar(hangar: Hangar): Observable<any> {
    return this.http.post(this.url + "saveHangar", hangar);
  }

  deleteHangar(id: number): Observable<any>{
    return this.http.delete(this.url + "deleteHangar?id=" + id);
  }

  deletePilot(id: number): Observable<any>{
    return this.http.delete(this.url + "deletePilot?id=" + id);
  }

  deleteTransport(id: string): Observable<any>{
    return this.http.delete(this.url + "deleteTransport?id=" + id);
  }

  getHangarsByType(type: string): Observable<Hangar[]> {
    return this.http.get<Hangar[]>(this.url + "findHangarsByType?type=" + type);
  }

  getPilotsWithAdmins(): Observable<Pilot[]> {
    return this.http.get<Pilot[]>(this.url + "findAllPilotsWithAdmins");
  }

}
