import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Transport} from "../models/Transport";
import {Observable} from "rxjs";
import {Hangar} from "../models/Hangar";
import {Pilot} from "../models/Pilot";
import {Schedule} from "../models/Schedule";

@Injectable({
  providedIn: "root",
})
export class AirportService {
  url = "http://localhost:8081/api/"; // юрл для юзеров

  constructor(private http: HttpClient) { }

  getTransports(): Observable<Transport[]> {
    return this.http.get<Transport[]>(this.url + "findAllTransports");
  }

  getHangars(): Observable<Hangar[]> {
    return this.http.get<Hangar[]>(this.url + "findAllHangars");
  }


  getPilots(): Observable<Pilot[]> {
    return this.http.get<Pilot[]>(this.url + "findAllPilots");
  }

  getSchedule(): Observable<Schedule[]> {
    return this.http.get<Schedule[]>(this.url + "findAllSchedules");
  }

  schedule(schedule: { schedule: Schedule, tailNumber: string, employeeId: number }): Observable<Schedule> {
    return this.http.post<Schedule>(this.url + "schedule", schedule);
  }

}
