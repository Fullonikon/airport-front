import {Component, OnInit} from "@angular/core";
import {Transport} from "../../models/Transport";
import {Pilot} from "../../models/Pilot";
import {Hangar} from "../../models/Hangar";
import {AirportService} from "../../service/airport.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {

  title = "airport";
  date: Date;

  transports: Transport[] = [];
  pilots: Pilot[] = [];
  hangars: Hangar[] = [];

  constructor(private airportService: AirportService, private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.airportService.getTransports()
      .subscribe(res => this.transports = res);
    this.airportService.getPilots()
      .subscribe(res => this.pilots = res);
  }

  schedule(transport: Transport, pilot: Pilot): void {
    this.airportService.schedule({
      schedule: {
        departureTime: this.date
      },
      tailNumber: transport.tailNumber,
      employeeId: pilot.employeeId
    })
      .subscribe(res => console.log(res));
  }


  isAdmin(): boolean {
    return this.authService.getRole() === "ADMIN";
  }

  logOut(): void {
    this.authService.clear();
    this.router.navigateByUrl("/login");
  }

}
