import {Component, OnInit} from "@angular/core";
import {AirportService} from "../../service/airport.service";
import {Transport} from "../../models/Transport";
import {Hangar} from "../../models/Hangar";
import {Pilot} from "../../models/Pilot";
import {AdminService} from "../../service/admin.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";


@Component({
  selector: "app-hangar",
  templateUrl: "./hangar.component.html",
  styleUrls: ["./hangar.component.css"]
})
export class HangarComponent implements OnInit {
  title = "airport";
  date: Date;

  displayedColumns: string[] = ["HangarId", "Color", "Type"]; // названия колонок в таблице

  transports: Transport[] = [];
  pilots: Pilot[] = [];
  hangars: Hangar[] = [];

  constructor(private adminService: AdminService, private authService: AuthService, private router: Router,
              private airportService: AirportService) { }

  ngOnInit(): void {
    this.airportService.getHangars()
      .subscribe(res => this.hangars = res);
  }

  saveHangar(hangar: Hangar): void{
    this.adminService.saveHangar(hangar)
      .subscribe(res => this.hangars = [...this.hangars, res]); // перезапись массива ангаров чтобы таблица обновилась
  }

  deleteHangar(id: number): void{
    this.adminService.deleteHangar(id)
      .subscribe(res => {
        // tslint:disable-next-line:triple-equals
        this.hangars = this.hangars.filter(h => h.hangarId != id);
      });
  }

  logOut(): void {
    this.authService.clear();
    this.router.navigateByUrl("/login");
  }

}
