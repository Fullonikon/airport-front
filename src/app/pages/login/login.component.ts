import { Component, OnInit } from "@angular/core";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {pipe} from "rxjs";
import {catchError} from "rxjs/operators";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {

  isNotLogin = false;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(login: string, password: string): void {
    this.authService.auth(login, password)
      .subscribe(res => {
        if (res.accessToken) {
          this.router.navigateByUrl("/");
          this.authService.saveToken(res.accessToken);
        }
      },
        error => {
          this.isNotLogin = true;
        });
  }

}
