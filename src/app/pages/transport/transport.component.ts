import { Component, OnInit } from "@angular/core";
import {Transport} from "../../models/Transport";
import {AirportService} from "../../service/airport.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {Hangar} from "../../models/Hangar";
import {AdminService} from "../../service/admin.service";

@Component({
  selector: "app-transport",
  templateUrl: "./transport.component.html",
  styleUrls: ["./transport.component.css"]
})
export class TransportComponent implements OnInit {

  displayedColumns: string[] = ["tailNumber", "model", "type",
    "flightRange", "capacity", "Hangar"];
  transports: Transport[];
  hangars: Hangar[];
  hangarsOfType: Hangar[];
  planeType: string;

  constructor(private adminService: AdminService, private authService: AuthService, private router: Router,
              private airportService: AirportService) { }

  ngOnInit(): void {
    this.airportService.getTransports()
      .subscribe(res => this.transports = res);
    this.airportService.getHangars()
      .subscribe(res => this.hangars = res);
  }

  getHangarsByType(): void{
    console.log("planeType ", this.planeType);
    this.adminService.getHangarsByType(this.planeType)
      .subscribe(res => this.hangarsOfType = res);
  }

  saveTransport(transport: Transport, hangarId: string): void{
    this.adminService.saveTransport(transport, hangarId)
      .subscribe(res => this.transports = [...this.transports, res]);
  }

  deleteTransport(id: string): void{
    this.adminService.deleteTransport(id)
      .subscribe(res => {
        // @ts-ignore
        // tslint:disable-next-line:triple-equals
        this.transports = this.transports.filter(h => h.tailNumber != id);
      });
  }

  logOut(): void {
    this.authService.clear();
    this.router.navigateByUrl("/login");
  }


}
