import { Component, OnInit } from "@angular/core";
import {AirportService} from "../../service/airport.service";
import {Pilot} from "../../models/Pilot";
import {AdminService} from "../../service/admin.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-pilots",
  templateUrl: "./pilots.component.html",
  styleUrls: ["./pilots.component.css"]
})
export class PilotsComponent implements OnInit {
  pilots: Pilot[];

  displayedColumns: string[] = ["EmployeeId", "Category", "FirstName", "LastName", "Login", "Role"];

  constructor(private adminService: AdminService, private authService: AuthService, private router: Router,
              private airportService: AirportService) { }

  ngOnInit(): void {
    this.adminService.getPilotsWithAdmins()
      .subscribe(res => this.pilots = res);
  }

  savePilot(pilot: Pilot): void{
    this.adminService.savePilot(pilot)
      .subscribe(res => this.pilots = [...this.pilots, res]);
  }

  deletePilot(id: number): void{
    this.adminService.deletePilot(id)
      .subscribe(res => {
        // tslint:disable-next-line:triple-equals
        this.pilots = this.pilots.filter(h => h.employeeId != id);
      });
  }

  logOut(): void {
    this.authService.clear();
    this.router.navigateByUrl("/login");
  }
}
