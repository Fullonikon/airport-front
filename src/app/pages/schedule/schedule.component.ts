import { Component, OnInit } from "@angular/core";
import {AdminService} from "../../service/admin.service";
import {AuthService} from "../../service/auth.service";
import {Router} from "@angular/router";
import {AirportService} from "../../service/airport.service";
import {Schedule} from "../../models/Schedule";

@Component({
  selector: "app-schedule",
  templateUrl: "./schedule.component.html",
  styleUrls: ["./schedule.component.css"]
})
export class ScheduleComponent implements OnInit {

  schedules: Schedule[];

  displayedColumns: string[] = ["Flight Id", "Departure Time", "Pilot"];

  constructor(private adminService: AdminService, private authService: AuthService, private router: Router,
              private airportService: AirportService) { }

  ngOnInit(): void {
    this.airportService.getSchedule()
      .subscribe(res => this.schedules = res);
  }

  logOut(): void {
    this.authService.clear();
    this.router.navigateByUrl("/login");
  }

  isAdmin(): boolean {
    return this.authService.getRole() === "ADMIN";
  }

}
