import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {AuthService} from "../service/auth.service";

@Injectable()
export class HeaderInterceptor implements HttpInterceptor {


  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> { // интерсептер перехватывает все
                                                                                    // запросы и добавляет токен
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.authService.getToken()}`
    });
    req = req.clone({
      headers
    });
    return next.handle(req);
  }
}
